<?php

/* box.twig */
class __TwigTemplate_7e52f1640fbc8e82d05893e428c24e255d704642557e633907a1ef834fea8e5d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"wpml-box wpml-wc-box js-wpml-wc\">
\t\t<input type=\"hidden\" name=\"wpml_words_count_panel_nonce\" value=\"";
        // line 2
        if (isset($context["nonces"])) { $_nonces_ = $context["nonces"]; } else { $_nonces_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_nonces_, "wpml_words_count_panel_nonce", array()));
        echo "\">
\t\t<input type=\"hidden\" name=\"wpml_words_count_chunk_size\" value=\"";
        // line 3
        if (isset($context["wc_chunk_size"])) { $_wc_chunk_size_ = $context["wc_chunk_size"]; } else { $_wc_chunk_size_ = null; }
        echo twig_escape_filter($this->env, $_wc_chunk_size_);
        echo "\">

\t\t<div class=\"wpml-wc-messages\">
\t\t\t<p>";
        // line 6
        if (isset($context["strings"])) { $_strings_ = $context["strings"]; } else { $_strings_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_strings_, "message", array()), "html", null, true);
        echo "</p>
\t\t</div>
\t\t<div class=\"wpml-wc-buttons\">
\t\t\t<p>
\t\t\t\t<a class=\"button button-secondary js-wc-dialog-init\" href=\"";
        // line 10
        if (isset($context["strings"])) { $_strings_ = $context["strings"]; } else { $_strings_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_strings_, "openDialogButtonURL", array()), "html", null, true);
        echo "\" title=\"";
        if (isset($context["strings"])) { $_strings_ = $context["strings"]; } else { $_strings_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_strings_, "openDialogButton", array()));
        echo "\">
\t\t\t\t\t";
        // line 11
        if (isset($context["strings"])) { $_strings_ = $context["strings"]; } else { $_strings_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_strings_, "openDialogButton", array()), "html", null, true);
        echo "
\t\t\t\t</a>
\t\t\t</p>

\t\t\t<p>
\t\t\t\t<small>
\t\t\t\t\t";
        // line 17
        if (isset($context["strings"])) { $_strings_ = $context["strings"]; } else { $_strings_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_strings_, "callToAction", array()), "Text", array()), "html", null, true);
        echo " <a href=\"";
        if (isset($context["strings"])) { $_strings_ = $context["strings"]; } else { $_strings_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_strings_, "callToAction", array()), "linkURL", array()), "html", null, true);
        echo "\" target=\"_blank\" class=\"wpml-external-link\" title=\"";
        if (isset($context["strings"])) { $_strings_ = $context["strings"]; } else { $_strings_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_strings_, "callToAction", array()), "linkText", array()));
        echo "\">";
        if (isset($context["strings"])) { $_strings_ = $context["strings"]; } else { $_strings_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_strings_, "callToAction", array()), "linkText", array()), "html", null, true);
        echo "</a>
\t\t\t\t</small>
\t\t\t</p>
\t\t</div>
\t\t";
        // line 21
        if (isset($context["dialog"])) { $_dialog_ = $context["dialog"]; } else { $_dialog_ = null; }
        $this->loadTemplate("dialog.twig", "box.twig", 21)->display(array_merge($context, $_dialog_));
        // line 22
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "box.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 22,  77 => 21,  60 => 17,  50 => 11,  42 => 10,  34 => 6,  27 => 3,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"wpml-box wpml-wc-box js-wpml-wc\">
\t\t<input type=\"hidden\" name=\"wpml_words_count_panel_nonce\" value=\"{{ nonces.wpml_words_count_panel_nonce|e }}\">
\t\t<input type=\"hidden\" name=\"wpml_words_count_chunk_size\" value=\"{{ wc_chunk_size|e }}\">

\t\t<div class=\"wpml-wc-messages\">
\t\t\t<p>{{ strings.message }}</p>
\t\t</div>
\t\t<div class=\"wpml-wc-buttons\">
\t\t\t<p>
\t\t\t\t<a class=\"button button-secondary js-wc-dialog-init\" href=\"{{ strings.openDialogButtonURL }}\" title=\"{{ strings.openDialogButton|e }}\">
\t\t\t\t\t{{ strings.openDialogButton }}
\t\t\t\t</a>
\t\t\t</p>

\t\t\t<p>
\t\t\t\t<small>
\t\t\t\t\t{{ strings.callToAction.Text }} <a href=\"{{ strings.callToAction.linkURL }}\" target=\"_blank\" class=\"wpml-external-link\" title=\"{{ strings.callToAction.linkText|e }}\">{{ strings.callToAction.linkText }}</a>
\t\t\t\t</small>
\t\t\t</p>
\t\t</div>
\t\t{% include 'dialog.twig' with dialog %}
</div>", "box.twig", "/var/www/vhosts/caboel.es/httpdocs/wp-caboel/wp-content/plugins/wpml-translation-management/templates/words-count/box.twig");
    }
}
