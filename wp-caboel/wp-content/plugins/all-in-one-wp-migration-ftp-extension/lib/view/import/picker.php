<?php
/**
 * Copyright (C) 2014-2018 ServMask Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ███████╗███████╗██████╗ ██╗   ██╗███╗   ███╗ █████╗ ███████╗██╗  ██╗
 * ██╔════╝██╔════╝██╔══██╗██║   ██║████╗ ████║██╔══██╗██╔════╝██║ ██╔╝
 * ███████╗█████╗  ██████╔╝██║   ██║██╔████╔██║███████║███████╗█████╔╝
 * ╚════██║██╔══╝  ██╔══██╗╚██╗ ██╔╝██║╚██╔╝██║██╔══██║╚════██║██╔═██╗
 * ███████║███████╗██║  ██║ ╚████╔╝ ██║ ╚═╝ ██║██║  ██║███████║██║  ██╗
 * ╚══════╝╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚═╝     ╚═╝╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝
 */
?>

<div id="ai1wmfe-import-modal" class="ai1wmfe-modal-container">
	<div class="ai1wmfe-modal-content" v-if="files !== false">
		<div class="ai1wmfe-file-browser">
			<span class="ai1wmfe-path">
				<i class="ai1wm-icon-folder"></i>
				<span id="ai1wmfe-download-path">{{ path }}</span>
			</span>
			<ul class="ai1wmfe-file-list">
				<li v-repeat="files" v-on="click: browse(this)" class="ai1wmfe-file-item">
					<span class="ai1wmfe-filename">
						<i class="{{ type | icon }}"></i>
						{{ name }}
					</span>
					<span class="ai1wmfe-filedate" v-if="type !== 'folder'">{{ date }}</span>
					<span class="ai1wmfe-filesize" v-if="type !== 'folder'">{{ size }}</span>
				</li>
			</ul>
			<p class="ai1wmfe-file-row" v-if="files.length === 0 && num_hidden_files === 0">
				<?php _e( 'No files or directories', AI1WMFE_PLUGIN_NAME ); ?>
			</p>
			<p class="ai1wmfe-file-row" v-if="num_hidden_files === 1">
				{{ num_hidden_files }}
				<?php _e( 'file is hidden', AI1WMFE_PLUGIN_NAME ); ?>
				<i class="ai1wm-icon-help ai1wm-tooltip">
					<span><?php _e( 'Only wpress backups and folders are visible', AI1WMFE_PLUGIN_NAME ); ?></span>
				</i>
			</p>
			<p class="ai1wmfe-file-row" v-if="num_hidden_files > 1">
				{{ num_hidden_files }}
				<?php _e( 'files are hidden', AI1WMFE_PLUGIN_NAME ); ?>
				<i class="ai1wm-icon-help ai1wm-tooltip">
					<span><?php _e( 'Only wpress backups and folders are visible', AI1WMFE_PLUGIN_NAME ); ?></span>
				</i>
			</p>
		</div>
	</div>

	<div class="ai1wmfe-modal-loader" v-if="files === false">
		<p>
			<span style="float: none; visibility: visible;" class="spinner"></span>
		</p>
		<p>
			<span class="ai1wmfe-contact-ftp">
				<?php printf( __( 'Connecting to %s server ...', AI1WMFE_PLUGIN_NAME ), strtoupper( $type ) ); ?>
			</span>
		</p>
	</div>

	<div class="ai1wmfe-modal-action">
		<p>
			<span id="ai1wmfe-download-file" class="ai1wmfe-selected-file" v-if="selected_filename" v-animation>
				<i class="ai1wm-icon-file-zip"></i>
				{{ selected_filename }}
			</span>
		</p>
		<p>
			<button type="button" class="ai1wm-button-red" id="ai1wmfe-import-file-cancel" v-on="click: cancel">
				<i class="ai1wm-icon-notification"></i>
				<?php _e( 'Cancel', AI1WMFE_PLUGIN_NAME ); ?>
			</button>
			<button type="button" class="ai1wm-button-green" id="ai1wmfe-import-file" v-if="selected_filename" v-on="click: import">
				<i class="ai1wm-icon-publish"></i>
				<?php _e( 'Import', AI1WMFE_PLUGIN_NAME ); ?>
			</button>
		</p>
	</div>
</div>

<div id="ai1wmfe-import-overlay" class="ai1wmfe-overlay"></div>
