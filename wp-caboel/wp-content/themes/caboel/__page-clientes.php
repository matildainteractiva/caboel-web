<?php
/*
 Template Name: Clientes
*/
?>
<?php get_header(); ?>
<?php
	if (function_exists('icl_object_id')){
		$id_contacta_page = icl_object_id(10,'page',true);
	}else{
		$id_contacta_page = 10;
	}
?>
			<div id="content" class="clientes-page">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<div class="module clientes-mod cf">
									<header class="article-header">
										<h1 class="page-title"><span><?php the_title(); ?></span></h1>
										<div class="intro"><?php the_content(); ?></div>
									</header>
									
									
	
									
									<?php
									//MOD LISTA CLIENTES
									$args = array(
										'posts_per_page' => -1,
										'post_type' => 'cliente'
									);
									$the_query = new WP_Query($args);
									if ($the_query->have_posts()): //si hi ha algun slide, mostrem el slider, sino, no el mostrem
									?>
													
									<ul class="list-clientes">
									<?php
									//llistem tots els posts del tipus proyectos
									while($the_query->have_posts()) : $the_query->the_post();
										$title = get_the_title();
									?>		
										<li class="logo animation" data-os-animation="zoomIn" data-os-animation-delay=".8">
											<?php echo types_render_field('logotipo',array('alt' => $title ,'size'=>'activo')); ?>										
										</li>
									<?php
									endwhile;
									?>
									</ul>
									
									<?php
									endif;
									wp_reset_postdata(); // reset the query
									//END LISTA CLIENTES
									?>
								</div>

								
								<?php
								//MOD CONTACTA
								?>
								<section class="module link-mod cf">
									<h2 class="mod-title"><?php _e('¿Quiere contactar con nosotros?','caboel');?></h2>
									<a href="<?php echo get_permalink($id_contacta_page); ?>" class="btn"><?php _e('Contacta','caboel');?></a>
								</section>								
								<?php
								//END MOD CONTACTA
								?>
							
								
								

								

							</article>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page-custom.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</main>


				</div>

			</div>


<?php get_footer(); ?>
