<?php /*
	   * LLISTAT ACTIUS D'UNA CATEGORIA
	   * Utilitzarem aquest template per mostrar els llistats d'actius de cada categoria, ja que no el necessitem per a res m�s en aquesta web.
	   * Tindrem 2 templates diferents, un per a "Comercial" i l'altre per a resta de categories
	   * Primer mirarem a quina categoria estem i en funci� d'aix� mostrem un template o l'altre
	   **/
?>
<?php get_header(); ?>
<?php

	$term_id = get_queried_object()->term_id;
	
	if (function_exists('icl_object_id')){
		$id_activos_page = icl_object_id(14,'page',true);
		$term_id = icl_object_id($term_id,'tipo-activo',true);
	}else{
		$id_activos_page = 14;
	}
	
	$id_comercial_term = icl_object_id(3,'tipo-activo',true);

?>
			<div id="content" class="activos-cat">

				<div id="inner-content" class="wrap cf">
						
						
						<?php
						
						if($term_id == $id_comercial_term ){ //************************************* TEMPLATE CATEGORIA "COMERCIAL"  *******************************************************
							$args = array(
								'parent' => $term_id,
								'hide_empty' => false
							);
							$terms = get_terms('tipo-activo',$args);
							$posts = array();
						?>
						
						<main id="main" class="cat-comercial cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
							
							<div class="page-header cf">
								<div class="title-and-claim">
									<?php the_archive_title( '<h1 class="page-title"><span>', '</span></h1>' ); ?>
									<h2 class="claim"><?php echo types_render_termmeta("claim-categoria", array() );?></h2>
								</div>
								<?php // the_archive_description( '<div class="taxonomy-description">', '</div>' ); ?>
								<ul class="list-cat-activos">
									<?php	
										foreach ( $terms as $term ) {
											$term_name = $term->name;
											$term_slug = $term->slug;
									?>
									<li>
										<a href="#<?php echo $term_slug; ?>"><?php echo $term_name; ?></a>
									</li>
									<?php } ?>
								</ul>
								<div class="back-container">
									<a class="back" href="<?php echo get_permalink($id_activos_page); ?>"><?php _e('Volver a Activos','caboel'); ?></a>
								</div>
							</div>
							
							
							
							<ul class="list-activos">
							
							<?php
							foreach ( $terms as $term ) {								
								$term_name = $term->name;
								$term_slug = $term->slug;
								$term_description = $term->description;
								$term_image_url = get_term_meta($term->term_id, "wpcf-imagen-categoria",true );
								$posts[$term->name] = get_posts(array( 'posts_per_page' => -1, 'post_type' => 'activo', 'tipo-activo' => $term->name ));
							?>
								<li class="cat-item cf" id="<?php echo $term_slug; ?>">
									<div class="cat-header cf">
										<div class="title-and-claim">
											<h2 class="title"><span><?php echo $term_name; ?></span></h2>
										</div>
										<div class="taxonomy-description"><?php echo $term_description;?></div>
										
									</div>
										
									<div class="list-index cf">
										<ul class="activos-index">
											<?php	
												foreach ( $posts[$term->name] as $post_comercial ) {
													$post_ID = $post_comercial->ID;
													$post_slug = $post_comercial->post_name;
													$post_title = get_the_title($post_ID);
													$post_direccion = get_post_meta($post_ID, "wpcf-direccion-activo",true);
											?>
											<li>
												<a href="#<?php echo $post_slug; ?>"><?php echo $post_title; ?> <span><?php echo $post_direccion; ?></span></a>
											</li>
											<?php } ?>
										</ul>
										<div class="image">
											<img src="<?php echo $term_image_url; ?>" alt="<?php echo $term_name; ?>">
										</div>
									</div>	
								
								<?php	
									foreach ( $posts[$term->name] as $post_comercial ) {
										$post_ID = $post_comercial->ID;
										$post_slug = $post_comercial->post_name;
										$post_title = get_the_title($post_ID);
										$post_direccion = get_post_meta($post_ID, "wpcf-direccion-activo",true);
										$post_image_url = get_post_meta($post_ID, "wpcf-imagen-activo",true);
										
								?>
	
									<article id="<?php echo $post_slug; ?>" class="activo cf" role="article">		
										<div class="data">
											<h3 class="h4 title"><?php echo $post_title; ?> <span><?php echo $post_direccion; ?></span></h3>
										</div>
										<div class="image os-animation" data-os-animation="fadeInUp" data-os-animation-delay=".1">
											<div class="cycle-slideshow" data-cycle-slides=">img" data-cycle-swipe="true" data-cycle-timeout="4000" data-cycle-pause-on-hover="true" data-cycle-log="false" data-cycle-prev=".prev" data-cycle-next=".next">
												<img src="<?php echo $post_image_url; //pot ser que mostri m�s d'una imatge ?>" alt="<?php echo $post_title; ?>">
											</div>
				
											<div class="controls">
												<a class="prev" title="<?php _e('Previous','caboel');?>"></a>
												<a class="next" title="<?php _e('Next','caboel');?>"></a>
											</div>
										
											<!-- empty element for pager links -->
											<div class="cycle-pager"></div>
										</div>
									</article>
									
	
								<?php
									}
								?>
								</li>
							<?php
							}
							?>
							
							
							</ul>
						</main>
						
						<?php
						// end categoria "Comercial"
						 }else{ //********************************************** TEMPLATE RESTA CATEGORIES  *********************************************************************
							
						?>
					
						<main id="main" class="cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
							<div class="page-header cf">
								<div class="title-and-claim">
									<?php the_archive_title( '<h1 class="page-title"><span>', '</span></h1>' ); ?>
									<h2 class="claim"><?php echo types_render_termmeta("claim-categoria", array() );?></h2>
								</div>
								<?php the_archive_description( '<div class="taxonomy-description">', '</div>' ); ?>
								<div class="back-container">
									<a class="back" href="<?php echo get_permalink($id_activos_page); ?>"><?php _e('Volver a Activos','caboel'); ?></a>
								</div>
							</div>
							
							<div class="list-index cf">
								<div class="image">
									<?php echo types_render_termmeta("imagen-categoria", array( "alt" => "", "size" => "activo") );?>
								</div>
								<ul id="activos-index">
									<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
									<li>
										<a href="#post-<?php the_ID(); ?>"><?php the_title(); ?> <span><?php echo types_render_field('direccion-activo',array()); ?></span></a>
									</li>
									<?php endwhile; endif; ?>
								</ul>
							</div>
							
							<div class="list-activos">
							
							<?php								
								if (have_posts()) : while (have_posts()) : the_post();
							?>

								<article id="post-<?php the_ID(); ?>" <?php post_class( 'activo cf' ); ?> role="article">								
									<div class="image os-animation" data-os-animation="fadeInUp" data-os-animation-delay=".3">
										<div class="cycle-slideshow" data-cycle-slides=">img" data-cycle-swipe="true" data-cycle-timeout="4000" data-cycle-pause-on-hover="false" data-cycle-log="false" data-cycle-prev=".prev" data-cycle-next=".next">
											<?php echo types_render_field('imagen-activo',array('size'=>'activo')); //pot ser que mostri m�s d'una imatge ?>
										</div>
			
										<div class="controls">
											<a class="prev" title="<?php _e('Previous','caboel');?>"></a>
											<a class="next" title="<?php _e('Next','caboel');?>"></a>
										</div>
									
										<!-- empty element for pager links -->
										<div class="cycle-pager"></div>
									</div>
									<div class="data os-animation" data-os-animation="fadeInUp" data-os-animation-delay=".1">
										<h3 class="h2 title"><?php the_title(); ?> <span><?php echo types_render_field('direccion-activo',array()); ?></span></h3>
										<div class="description">
											<?php echo types_render_field('descripcion-activo',array()); ?>
										</div>
									</div>
								</article>

							<?php endwhile;endif; ?>							
							
							</div>
						</main>

						<?php } //end resta categories ?>		

						

				</div>

			</div>

<?php get_footer(); ?>
