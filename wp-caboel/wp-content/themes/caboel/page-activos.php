<?php
/*
 Template Name: Activos
*/
?>
<?php get_header(); ?>
<?php
	if (function_exists('icl_object_id')){
		$id_asistencial = icl_object_id(28,'tipo-activo',true);
		$id_comercial = icl_object_id(3,'tipo-activo',true);
		$id_hotelero = icl_object_id(4,'tipo-activo',true);
		$id_logistico = icl_object_id(5,'tipo-activo',true);
		$id_oficinas = icl_object_id(6,'tipo-activo',true);
		$id_proyectos_page = icl_object_id(16,'page',true);
	}else{
		$id_asistencial = 28;
		$id_comercial = 3;
		$id_hotelero = 4;
		$id_logistico = 5;
		$id_oficinas = 6;
		$id_proyectos_page = 16;
	}
?>

			<div id="content" class="activos-page">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header" style="background-image: url(<?php the_post_thumbnail_url('proyecto'); ?> )">
									<div class="article-header-inner">
										<h1 class="page-title"><span><?php the_title();?></span></h1>
										<div class="intro"><?php the_content(); ?></div>
									</div>
								</header>
								
								

								
								<?php
								//MOD LISTA CATEGORÍAS ACTIVOS
								?>
								<section class="module list-activos-mod cf">
									<ul>
										<li class="item comercial-item linkable animation" data-os-animation="fadeInUp2" data-os-animation-delay=".2s">
											<?php $comercial_array = get_term_by('id', $id_comercial, 'tipo-activo', 'ARRAY_A'); ?>
											<h2 class="title"><?php print_r($comercial_array['name']); ?></h2>
											<div class="description">
												<?php print_r($comercial_array['description']); ?>
											</div>
											<div class="img-container">
												<img src="<?php bloginfo('template_url'); ?>/library/images/activos/comercial.jpg" alt="<?php print_r($comercial_array['name']); ?>">
												<div class="img-hover">
													<div class="img-hover-inner">
														<div class="img-hover-inner2">
															<p><?php _e('Locales comerciales','caboel'); ?></p>
															<p><?php _e('Medianas superficies','caboel'); ?></p>
															<p><?php _e('Estaciones de serivicio','caboel'); ?></p>
														</div>
													</div>
												</div>
											</div>
											<a href="<?php echo get_term_link($id_comercial, 'tipo-activo'); ?>" class="btn"><span><?php _e('Ver activos','caboel');?></span></a>
										</li>
										<li class="item hotelero-item linkable animation" data-os-animation="fadeInUp2" data-os-animation-delay=".4s">
											<?php $hotelero_array = get_term_by('id', $id_hotelero, 'tipo-activo', 'ARRAY_A'); ?>
											<h2 class="title"><?php print_r($hotelero_array['name']); ?></h2>
											<div class="description">
												<?php print_r($hotelero_array['description']); ?>
											</div>
											<div class="img-container">
												<img src="<?php bloginfo('template_url'); ?>/library/images/activos/hotelero.jpg" alt="<?php print_r($hotelero_array['name']); ?>">
												<div class="img-hover">
													<div class="img-hover-inner">
														<div class="img-hover-inner2">
															<p><?php _e('Hoteles urbanos','caboel'); ?></p>
														</div>
													</div>
												</div>
											</div>
											<a href="<?php echo get_term_link($id_hotelero, 'tipo-activo'); ?>" class="btn"><span><?php _e('Ver activos','caboel');?></span></a>
										</li>
										<li class="item logistico-item linkable animation" data-os-animation="fadeInUp2" data-os-animation-delay=".6s">
											<?php $logistico_array = get_term_by('id', $id_logistico, 'tipo-activo', 'ARRAY_A'); ?>
											<h2 class="title"><?php print_r($logistico_array['name']); ?></h2>
											<div class="description">
												<?php print_r($logistico_array['description']); ?>
											</div>
											<div class="img-container">
												<img src="<?php bloginfo('template_url'); ?>/library/images/activos/logistico.jpg" alt="<?php print_r($logistico_array['name']); ?>">
												<div class="img-hover">
													<div class="img-hover-inner">
														<div class="img-hover-inner2">
															<p><?php _e('Plataformas logísticas','caboel'); ?></p>
														</div>
													</div>
												</div>
											</div>
											<a href="<?php echo get_term_link($id_logistico, 'tipo-activo'); ?>" class="btn"><span><?php _e('Ver activos','caboel');?></span></a>
										</li>
										<li class="item oficinas-item linkable animation" data-os-animation="fadeInUp2" data-os-animation-delay=".8s">
											<?php $oficinas_array = get_term_by('id', $id_oficinas, 'tipo-activo', 'ARRAY_A'); ?>
											<h2 class="title"><?php print_r($oficinas_array['name']); ?></h2>
											<div class="description">
												<?php print_r($oficinas_array['description']); ?>
											</div>
											<div class="img-container">
												<img src="<?php bloginfo('template_url'); ?>/library/images/activos/oficinas.jpg" alt="<?php print_r($oficinas_array['name']); ?>">
												<div class="img-hover">
													<div class="img-hover-inner">
														<div class="img-hover-inner2">
															<p><?php _e('Oficinas en Barcelona, Madrid, Lisboa y Oporto','caboel');?></p>
														</div>
													</div>
												</div>
											</div>
											<a href="<?php echo get_term_link($id_oficinas, 'tipo-activo'); ?>" class="btn"><span><?php _e('Ver activos','caboel');?></span></a>
										</li>
										<li class="item oficinas-item linkable animation" data-os-animation="fadeInUp2" data-os-animation-delay=".8s">
											<?php $asistencial_array = get_term_by('id', $id_asistencial, 'tipo-activo', 'ARRAY_A'); ?>
											<h2 class="title"><?php print_r($asistencial_array['name']); ?></h2>
											<div class="description">
												<?php print_r($asistencial_array['description']); ?>
											</div>
											<div class="img-container">
												<img src="<?php bloginfo('template_url'); ?>/library/images/activos/asistencial-2.jpg" alt="<?php print_r($asistencial_array['name']); ?>">
												<div class="img-hover">
													<div class="img-hover-inner">
														<div class="img-hover-inner2">
															<p><?php _e('Edificios en la CC.AA. de Madrid','caboel');?></p>
														</div>
													</div>
												</div>
											</div>
											<a href="<?php echo get_term_link($id_asistencial, 'tipo-activo'); ?>" class="btn"><span><?php _e('Ver activos','caboel');?></span></a>
										</li>
									</ul>
								</section>								
								<?php
								//END LISTA CATEGORÍAS ACTIVOS
								?>

								
								<?php
								//MOD VER PROYECTOS
								?>
								<section class="module link-mod cf os-animation" data-os-animation="fadeInUp" data-os-animation-delay=".2">
									<h2 class="mod-title"><?php _e('¿Quiere conocer en qué proyectos estamos trabajando?','caboel');?></h2>
									<a href="<?php echo get_permalink($id_proyectos_page); ?>" class="btn"><span><?php _e('Ver proyectos','caboel');?></span></a>
								</section>								
								<?php
								//END MOD VER PROYECTOS
								?>
							
								
								

								

							</article>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page-custom.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</main>


				</div>

			</div>


<?php get_footer(); ?>
