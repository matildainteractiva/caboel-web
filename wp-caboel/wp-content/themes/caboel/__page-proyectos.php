<?php
/*
 Template Name: Proyectos
*/
?>
<?php get_header(); ?>
<?php
	if (function_exists('icl_object_id')){
		$id_clientes_page = icl_object_id(18,'page',true);
	}else{
		$id_clientes_page = 18;
	}
?>
			<div id="content" class="proyectos-page">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header">
									<h1 class="page-title"><span><?php the_title(); ?></span></h2>
									<div class="intro"><div class="intro-inner"><?php the_content(); ?></div></div>
								</header>
								
								

								
								<?php
								//MOD LISTA PROYECTOS
								$args = array(
									'posts_per_page' => -1,
									'post_type' => 'proyecto'
								);
								$the_query = new WP_Query($args);
								if ($the_query->have_posts()): //si hi ha algun slide, mostrem el slider, sino, no el mostrem
								?>
								<section class="proyectos-mod cf">							
									
									<?php
									//llistem tots els posts del tipus proyectos
									while($the_query->have_posts()) : $the_query->the_post();	
									?>
									<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">								
										<div class="mod-header animation" data-os-animation="fadeInLeft" data-os-animation-delay=".1s">
											<h2 class="mod-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
										
											<dl class="info-proyecto">
												<?php if(types_render_field('objetivo-proyecto',array())): ?>
												<dt class="objetivo"><?php _e('Objetivo','caboel'); ?></dt>
												<dd class="objetivo"><?php echo types_render_field('objetivo-proyecto',array()); ?></dd>
												<?php endif;?>
												<?php if(types_render_field('inicio-proyecto',array())): ?>
												<dt class="inicio"><?php _e('Inicio del proyecto','caboel'); ?></dt>
												<dd class="inicio"><?php echo types_render_field('inicio-proyecto',array()); ?></dd>
												<?php endif;?>
												
												<?php if(types_render_field('finalizacion-proyecto',array())): ?>
												<dt class="finalizacion"><?php _e('Finalización','caboel'); ?></dt>
												<dd class="finalizacion"><?php echo types_render_field('finalizacion-proyecto',array()); ?></dd>
												<?php endif;?>
											</dl>
											
											<a href="<?php the_permalink(); ?>" class="btn"><?php _e('Ver proyecto','caboel');?></a>
										</div>
										<div class="image animation" data-os-animation="fadeInUp" data-os-animation-delay=".1s">
											<a href="<?php the_permalink(); ?>"><?php echo types_render_field('imagen-inicio-proyecto',array('size'=>'proyecto')); ?></a>									
										</div>
										
									</article>
									
									<?php
									endwhile;
									?>
									
								</section><!-- .proyectos-mod -->
								<?php
								endif;
								wp_reset_postdata(); // reset the query
								//END LISTA PROYECTOS
								?>

								
								<?php
								//MOD VER CLIENTES
								?>
								<section class="module link-mod cf os-animation" data-os-animation="fadeInUp" data-os-animation-delay=".1s">
									<h2 class="mod-title">¿Quieres conocer a nuestros clientes?</h2>
									<a href="<?php echo get_permalink($id_clientes_page); ?>" class="btn"><?php _e('Nuestros clientes','caboel');?></a>
								</section>								
								<?php
								//END MOD VER CLIENTES
								?>
							
								
								

								

							</article>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page-custom.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</main>


				</div>

			</div>


<?php get_footer(); ?>
