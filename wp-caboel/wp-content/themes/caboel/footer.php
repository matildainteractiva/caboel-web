			
				<div id="push"></div>
			</div>
			
			<footer class="footer" id="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

				<div id="inner-footer" class="wrap cf">

					<nav role="navigation">
						<?php wp_nav_menu(array(
    					'container' => 'div',                           // enter '' to remove nav container (just make sure .footer-links in _base.scss isn't wrapping)
    					'container_class' => 'footer-links cf',         // class of container (should you choose to use it)
    					'menu' => __( 'Footer Links', 'bonestheme' ),   // nav name
    					'menu_class' => 'nav footer-nav cf',            // adding custom nav class
    					'theme_location' => 'footer-links',             // where it's located in the theme
    					'before' => '',                                 // before the menu
    					'after' => '',                                  // after the menu
    					'link_before' => '',                            // before each link
    					'link_after' => '',                             // after each link
    					'depth' => 0,                                   // limit the depth of the nav
    					'fallback_cb' => 'bones_footer_links_fallback'  // fallback function
						)); ?>
					</nav>
					
					<?php
						if (function_exists('icl_object_id')){
							$id_legal_page = icl_object_id(12,'page',true);
						}else{
							$id_legal_page = 12;
						}
					?>
					

					<div class="source-org copyright">
						<p>&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?></p>
						<p>
							<a href="mailto:info@caboel.es">info@caboel.es</a>
							<a href="<?php echo get_permalink($id_legal_page); ?>"><?php _e('Aviso legal','caboel'); ?></a>
						</p>
					</div>
					<!--
					<div class="source-org web-by">
						<p><a href="http://www.matilda-interactiva.com/" target='_blank'>Web by M!</a></p>
					</div>
					-->
				</div>

			</footer>

		

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->
