<?php
/*
 Template Name: Home
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header animation" data-os-animation="fadeInUp" data-os-animation-delay="0">
									<div class="claim"><?php the_content(); ?></div>

								</header>
								
								<?php
									if (function_exists('icl_object_id')){
										$id_contacta_page = icl_object_id(10,'page',true);
										$id_compania_page = icl_object_id(8,'page',true);
										$id_activos_page = icl_object_id(14,'page',true);
										$id_proyectos_page = icl_object_id(16,'page',true);
										$id_comercial = icl_object_id(3,'tipo-activo',true);
										$id_hotelero = icl_object_id(4,'tipo-activo',true);
										$id_logistico = icl_object_id(5,'tipo-activo',true);
										$id_oficinas = icl_object_id(6,'tipo-activo',true);
										$id_asistencial = icl_object_id(28,'tipo-activo',true);
									}else{
										$id_contacta_page = 10;
										$id_compania_page = 8;
										$id_activos_page = 14;
										$id_proyectos_page = 16;
										$id_comercial = 3;
										$id_hotelero = 4;
										$id_logistico = 5;
										$id_oficinas = 6;
										$id_asistencial = 28;
									}
								?>
								

								<?php
								//MOD SLIDER
								$args = array(
									'posts_per_page' => -1,
									'post_type' => 'slide-inicio'
								);
								$the_query = new WP_Query($args);
								if ($the_query->have_posts()): //si hi ha algun slide, mostrem el slider, sino, no el mostrem
								?>
								<section class="module slider-mod cf">
									<div class="mod-inner">
										<div class="cycle-slideshow" data-cycle-slides=">.slide" data-cycle-swipe="true" data-cycle-timeout="4000" data-cycle-pause-on-hover="false" data-cycle-log="false" data-cycle-prev=".prev" data-cycle-next=".next">
											<?php
											//llistem tots els posts del tipus Slide Home
											while($the_query->have_posts()) : $the_query->the_post();											
												
												//mirem si existei la imatge	
												if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
													$thumb_id = get_post_thumbnail_id();
													$thumb_url = wp_get_attachment_image_src($thumb_id,'full', true);
												}else{
													$thumb_url="";		
												}
													
											?>
											<div class="slide entry-content" style="background-image:url()">
												<article class="slide-content" id="post-<?php the_ID(); ?>">
													<img src="<?php echo $thumb_url[0];?>" alt="Caboel">
												</article><!-- .slide-content -->
											</div><!-- .slide -->
											
											<?php
											endwhile;
											?>
										</div>
			
										<div class="controls">
											<a class="prev" title="<?php _e('Previous','caboel');?>"></a>
											<a class="next" title="<?php _e('Next','caboel');?>"></a>
										</div>
								
										<!-- empty element for pager links -->
										<div class="cycle-pager"></div>
									</div>
								</section><!-- .slider-mod -->
								<?php
								endif;
								wp_reset_postdata(); // reset the query
								//END MOD SLIDER
								?>
								
								<?php
								//MOD COMPAÑÍA
								?>
								<section class="module compania-mod cf">
									<div class="mod-header">
										<h2 class="h1 mod-title"><?php echo types_render_field('titulo-compania',array()); ?></h2>
										<div class="description desc1">
											<?php echo types_render_field('texto-1-compania',array()); ?>
										</div>
									</div>
									<div class="image image1 os-animation" data-os-animation="fadeInLeft" data-os-animation-delay=".2s">
										<img src="<?php bloginfo('template_url'); ?>/library/images/home/home-compania.jpg" alt="Compañía">
									</div>
									<div class="image image2 os-animation" data-os-animation="fadeInRight" data-os-animation-delay=".2s">
										<img src="<?php bloginfo('template_url'); ?>/library/images/home/home-compania-sm-2.jpg" alt="Compañía">
									</div>
									<div class="description desc2 cf os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0">
										<h3 class="title"><?php echo types_render_field('titulo-texto-2-compania',array()); ?></h3>
										<?php echo types_render_field('texto-2-compania',array()); ?>
									</div>
									<div class="btns-container">
										<a href="<?php echo get_permalink($id_compania_page); ?>" class="btn"><span><?php _e('Quiero saber más','caboel');?></span></a>
									</div>
								</section>								
								<?php
								//END MOD COMPAÑIA
								?>
								
								<?php
								//MOD ACTIVOS
								?>
								<section class="module activos-mod cf">
									<div class="mod-header os-animation" data-os-animation="fadeInRight" data-os-animation-delay="0">
										<h2 class="h1 mod-title"><?php echo types_render_field('titulo-activos',array()); ?></h2>
										<div class="description desc1">
											<?php echo types_render_field('texto-1-activos',array()); ?>
										</div>
									</div>
									<div class="image hotelero-mod os-animation" data-os-animation="fadeInRight" data-os-animation-delay=".1s">
										<a href="<?php echo get_term_link($id_hotelero, 'tipo-activo'); ?>"><img src="<?php bloginfo('template_url'); ?>/library/images/home/home-hotelero.jpg" alt="<?php _e('Hoteles','caboel');?>"></a>
										<h3 class="title"><a href="<?php echo get_term_link($id_hotelero, 'tipo-activo'); ?>"><?php _e('Hoteles','caboel');?></a></h3>
									</div>
									<div class="resto-mod">
										<div class="image oficinas-mod os-animation" data-os-animation="fadeInLeft" data-os-animation-delay=".4s">
											<a href="<?php echo get_term_link($id_oficinas, 'tipo-activo'); ?>"><img src="<?php bloginfo('template_url'); ?>/library/images/home/home-oficinas.jpg" alt="<?php _e('Oficinas','caboel');?>"></a>
											<h3 class="title"><a href="<?php echo get_term_link($id_oficinas, 'tipo-activo'); ?>"><?php _e('Oficinas','caboel');?></a></h3>
										</div>
										<div class="image comercial-mod os-animation" data-os-animation="fadeInLeft" data-os-animation-delay=".3s">
											<a href="<?php echo get_term_link($id_comercial, 'tipo-activo'); ?>"><img src="<?php bloginfo('template_url'); ?>/library/images/home/home-comercial.jpg" alt="<?php _e('Comercial','caboel');?>"></a>
											<h3 class="title"><a href="<?php echo get_term_link($id_comercial, 'tipo-activo'); ?>"><?php _e('Comercial','caboel');?></a></h3>
										</div>
										<div class="image logistico-mod os-animation" data-os-animation="fadeInLeft" data-os-animation-delay=".2s">
											<a href="<?php echo get_term_link($id_logistico, 'tipo-activo'); ?>"><img src="<?php bloginfo('template_url'); ?>/library/images/home/home-logistico.jpg" alt="<?php _e('Logístico','caboel');?>"></a>
											<h3 class="title"><a href="<?php echo get_term_link($id_logistico, 'tipo-activo'); ?>"><?php _e('Logístico','caboel');?></a></h3>
										</div>
										<div class="image asistencial-mod os-animation" data-os-animation="fadeInLeft" data-os-animation-delay=".2s">
											<a href="<?php echo get_term_link($id_asistencial, 'tipo-activo'); ?>"><img src="<?php bloginfo('template_url'); ?>/library/images/home/home-asistencial.jpg" alt="<?php _e('Asistencial','caboel');?>"></a>
											<h3 class="title"><a href="<?php echo get_term_link($id_asistencial, 'tipo-activo'); ?>"><?php _e('Asistencial','caboel');?></a></h3>
										</div>
									</div>
									<div class="description desc2">
										<?php echo types_render_field('texto-2-activos',array()); ?>
									</div>
									<div class="btns-container">
										<a href="<?php echo get_permalink($id_activos_page); ?>" class="btn"><span><?php _e('Ver cartera de activos','caboel');?></span></a>
									</div>
								</section>								
								<?php
								//END MOD ACTIVOS
								?>
								
								<?php
								//MOD PROYECTOS
								?>
								<section class="module proyectos-mod cf">
									<div class="mod-header os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0">
										<h2 class="h1 mod-title"><?php echo types_render_field('titulo-proyectos',array()); ?></h2>
										<div class="description desc1">
											<?php echo types_render_field('texto-proyectos',array()); ?>
										</div>
									</div>
									<div class="image image1 os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0">
										<img src="<?php bloginfo('template_url'); ?>/library/images/home/home-proyectos-th1.jpg" alt="<?php _e('Proyectos','caboel');?>">
									</div>
									<div class="image image2 os-animation" data-os-animation="fadeInRight" data-os-animation-delay=".1s">
										<img src="<?php bloginfo('template_url'); ?>/library/images/home/home-proyectos-th2.jpg" alt="<?php _e('Proyectos','caboel');?>">
									</div>
									<div class="image image3 os-animation" data-os-animation="fadeInLeft" data-os-animation-delay=".3s">
										<img src="<?php bloginfo('template_url'); ?>/library/images/home/home-proyectos.jpg" alt="<?php _e('Proyectos','caboel');?>">
									</div>
									<div class="btns-container">
										<a href="<?php echo get_permalink($id_proyectos_page); ?>" class="btn"><span><?php _e('Ver proyectos','caboel');?></span></a>
									</div>
								</section>								
								<?php
								//END MOD PROYECTOS
								?>
								
								<?php
								//MOD CONTACTA
								?>
								<section class="module contacta-mod cf os-animation" data-os-animation="fadeInUp" data-os-animation-delay=".2s">
									<div class="mod-inner">
										<div class="mod-header">	
											<h2 class="mod-title"><?php echo types_render_field('titulo-contacta',array()); ?></h2>
										</div>
										<div class="description desc1">
											<?php echo types_render_field('texto-contacta',array()); ?>
										</div>
										<div class="description desc2">
											<p><?php _e('Nuestras oficinas se encuentran en:','caboel'); ?></p>
											<div class="address" itemscope itemtype="http://schema.org/Organization">
												<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
												<p itemprop="streetAddress">C/Josep Irla i Bosch, 1-3, 7ª planta</p>
												<p>
													<span itemprop="addressLocality">Barcelona</span>
													<span itemprop="postalCode">08034</span>
														
												</p>
												<meta itemprop="addressCountry" content="España">
												<meta itemprop="addressRegion" content="Catalunya">
												</div>
												Tel. <a href="tel:+34932058196" itemprop="telephone">+34 93 205 81 96</a><br>
												Fax. <a href="tel:+34932058197" itemprop="telephone">+34 93 205 81 97</a><br>
												<a href="mailto:info@caboel.es" itemprop="email">info@caboel.es</a><br>
												<meta itemprop="name" content="Caboel S.L.">
												<meta itemprop="image" content="<?php bloginfo('template_url');?>/library/images/logo-caboel.png">
												<meta itemprop="url" content="http://www.caboel.es/">
											</div>
										</div>
										<div class="btns-container">
											<a href="<?php echo get_permalink($id_contacta_page); ?>" class="btn"><span><?php _e('Contacta','caboel');?></span></a>
										</div>
									</div>
								</section>								
								<?php
								//END MOD CONTACTA
								?>
								


								

								

							</article>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page-custom.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</main>


				</div>

			</div>


<?php get_footer(); ?>
