<?php
/*
 Template Name: Compañía
*/
?>
<?php get_header(); ?>
<?php
	if (function_exists('icl_object_id')){
		$id_activos_page = icl_object_id(14,'page',true);
	}else{
		$id_activos_page = 14;
	}
?>
			<div id="content" class="compania-page">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="page-header" style="background-image: url(<?php the_post_thumbnail_url('large'); ?> )">
									<div class="article-header-inner">
										<div class="claim"><?php the_content(); ?></div>
									</div>
								</header>
																

								
								<?php
								//MOD COMPAÑÍA
								?>
								<section class="module compania-mod cf">
									<h1 class="h1 page-title"><span><?php the_title(); ?></span></h1>
									<div class="description desc1 columns2 cf">
										<div class="col1"><?php echo types_render_field('descripcion-compania-col1',array()); ?></div>
										<div class="col2"><?php echo types_render_field('descripcion-compania-col2',array()); ?></div>
									</div>
									<div class="block">
										<div class="description desc2">
											<h3 class="title"><?php echo types_render_field('titulo-bloque-1-compania',array()); ?></h3>
											<?php echo types_render_field('texto-bloque-1-compania-w',array()); ?>
										</div>
										<div class="image">
											<img src="<?php bloginfo('template_url'); ?>/library/images/compania/compania-planificacion.jpg" alt="Planificación">
										</div>
									</div>
									<div class="block">
										<div class="description desc2">
											<h3 class="title"><?php echo types_render_field('titulo-bloque-2-compania',array()); ?></h3>
											<?php echo types_render_field('texto-bloque-2-compania-w',array()); ?>
										</div>
										<div class="image">
											<img src="<?php bloginfo('template_url'); ?>/library/images/compania/compania-ejecucion.jpg" alt="Ejecución">
										</div>
									</div>
								</section>								
								<?php
								//END MOD COMPAÑIA
								?>
								
								<?php
								//MOD INVERSIÓN
								?>
								<section class="module compania-mod inversion-mod cf os-animation" data-os-animation="fadeInUp" data-os-animation-delay=".1">
									<h2 class="h1 page-title"><span><?php echo types_render_field('titulo-inversion',array()); ?></span></h2>
									<div class="description">
										<div class="columns2 cf">
											<div class="col1"><?php echo types_render_field('descripcion-inversion-col1',array()); ?></div>
											<div class="col2"><?php echo types_render_field('descripcion-inversion-col2',array()); ?></div>
										</div>
										<div class="image">
											<img src="<?php bloginfo('template_url'); ?>/library/images/compania/compania-inversion.jpg" alt="Inversión">
										</div>
									</div>
									
								</section>								
								<?php
								//END MOD INVERSIÓN
								?>
								
								<?php
								//MOD VER ACTIVOS
								?>
								<section class="module link-mod cf os-animation" data-os-animation="fadeInUp" data-os-animation-delay=".2">
									<h2 class="mod-title"><?php echo types_render_field('texto-veractivos',array()); ?></h2>
									<a href="<?php echo get_permalink($id_activos_page); ?>" class="btn"><?php _e('Ver activos','caboel');?></a>
								</section>								
								<?php
								//END MOD VER ACTIVOS
								?>
								
								

								

							</article>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page-custom.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</main>


				</div>

			</div>


<?php get_footer(); ?>
