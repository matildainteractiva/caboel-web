<?php
/*
 * SINGLE PROYECTO
 * Aquí mostrem les dades i fotos del projecte
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article">

								<header class="article-header">
									<div class="imagen-inicio">
										<?php echo types_render_field('imagen-inicio-proyecto',array('size'=>'proyecto')); ?>										
									</div>
									<h1 class="page-title single-title"><span><?php the_title(); ?></span></h1>

								</header>

								<section class="entry-content cf">
									
									
									<dl class="info-proyecto">
										<?php if(types_render_field('inicio-proyecto',array())): ?>
										<dt class="inicio"><?php _e('Inicio del proyecto','caboel'); ?></dt>
										<dd class="inicio"><?php echo types_render_field('inicio-proyecto',array()); ?></dd>
										<?php endif;?>
										
										<?php if(types_render_field('finalizacion-proyecto',array())): ?>
										<dt class="finalizacion"><?php _e('Finalización','caboel'); ?></dt>
										<dd class="finalizacion"><?php echo types_render_field('finalizacion-proyecto',array()); ?></dd>
										<?php endif;?>
										
										<?php if(types_render_field('objetivo-proyecto',array())): ?>
										<dt class="objetivo"><?php _e('Objetivo','caboel'); ?></dt>
										<dd class="objetivo"><?php echo types_render_field('objetivo-proyecto',array()); ?></dd>
										<?php endif;?>
										
										<?php if(types_render_field('entorno-proyecto',array())): ?>
										<dt class="entorno"><?php _e('Entorno','caboel'); ?></dt>
										<dd class="entorno"><?php echo types_render_field('entorno-proyecto',array()); ?></dd>
										<?php endif;?>
										
										<?php if(types_render_field('activo-proyecto',array())): ?>
										<dt class="activo"><?php _e('Activo','caboel'); ?></dt>
										<dd class="activo"><?php echo types_render_field('activo-proyecto',array()); ?></dd>
										<?php endif;?>
										
										<?php if(types_render_field('solucion-proyecto',array())): ?>
										<dt class="solucion"><?php _e('Solución adaptada','caboel'); ?></dt>
										<dd class="solucion"><?php echo types_render_field('solucion-proyecto',array()); ?></dd>
										<?php endif;?>
									</dl>
									
									<div class="imagenes-proceso cf">
										<?php echo types_render_field('imagen-en-proceso-1',array('size'=>'thumbnail')); ?>
										<?php echo types_render_field('imagen-en-proceso-2',array('size'=>'thumbnail')); ?>	
									</div>
									
									
									<?php
									//MOD PROPUESTA MÚLTIPLE
									$parentId = get_the_ID();
									$args = array(
										'posts_per_page' => -1,
										'post_type' => 'propuesta',
										'meta_query' => array(array('key' => '_wpcf_belongs_proyecto_id', 'value' => $parentId))
									);
									$the_query = new WP_Query($args);
									if ($the_query->have_posts()): //si hi ha alguna proposta, mostrem el mòdul, sino, no el mostrem
										$compt = 0;
									?>
									<div class="propuesta-mod cf os-animation" data-os-animation="fadeInUp" data-os-animation-delay=".1s">
										<h2 class="mod-title"><span><?php _e('Propuesta múltiple','caboel'); ?></span></h2>					
										<ul class="list-propuestas">
											<?php
											//llistem tots els posts del tipus proyectos
											while($the_query->have_posts()) : $the_query->the_post();
												$compt++;
											?>
											<li class="item" id="post-<?php the_ID(); ?>">
												<div class="num"><?php echo $compt;?></div>
												<h3 class="title"><?php the_title(); ?></h3>										
												<div class="objetivo">
													<?php echo types_render_field('descripcion-propuesta',array()); ?>
												</div>
											</li>										
											<?php
											endwhile;
											?>
										</ul>
									</div>
									<?php
									endif;
									wp_reset_postdata(); // reset the query
									//END PROPUESTA MÚLTIPLE
									?>
	
									<div class="imagen-finalizacion os-animation" data-os-animation="fadeInUp" data-os-animation-delay=".1s">
										<?php echo types_render_field('imagen-final-proyecto',array('size'=>'proyecto')); ?>
									</div>
										
									
								</section> <!-- end article section -->

								<footer class="article-footer">
								</footer>

							</article>

							<?php endwhile; ?>

							<?php else : ?>

									<article id="post-not-found" class="hentry cf">
										<header class="article-header">
											<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
											<p><?php _e( 'This is the error message in the single-custom_type.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</main>

						

				</div>

			</div>

<?php get_footer(); ?>
