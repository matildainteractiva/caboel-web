<?php get_header(); ?>
<?php
	if (function_exists('icl_object_id')){
		$id_home_page = icl_object_id(6,'page',true);
	}else{
		$id_home_page = 6;
	}
?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

					<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

						<article id="post-not-found" class="hentry cf module-error">

							<header class="article-header">

								<h1><?php _e( 'Aquí no hay previsto ningún proyecto', 'caboel' ); ?></h1>

							</header>

							<section class="entry-content">

								<p><?php _e( 'Quizás te has equivocado de página.', 'caboel' ); ?></p>

							</section>

							<section class="module cf animation" data-os-animation="fadeInUp" data-os-animation-delay=".1s">
									<h2 class="mod-title"><?php _e('Volver a la página de inicio','caboel');?></h2>
									<div class="description">
								
									</div>
									<a href="<?php echo get_permalink($id_home_page); ?>" class="btn"><?php _e('Inicio','caboel');?></a>
							</section>	

						

						</article>

					</main>

				</div>

			</div>

<?php get_footer(); ?>
