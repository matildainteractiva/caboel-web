/*
Scripts Caboel
Author: M! (http://matilda-interactiva.com)
*/

/* https://gist.github.com/anandkumar/11318494 */

jQuery(function($) {
	
	// When to show the scroll link
	// higher number = scroll link appears further down the page	
	var upperLimit = 100; 
		
	// Our scroll link element
	var scrollElem = $('a#scroll-to-top');
	
	// Scroll to top speed
	var scrollSpeed = 500;
	
	// Show and hide the scroll to top link based on scroll position	
	scrollElem.hide();
	jQuery(window).scroll(function () { 			
		var scrollTop = $(document).scrollTop();		
		if ( scrollTop > upperLimit ) {
			$(scrollElem).stop().fadeTo(300, 1); // fade back in			
		}else{		
			$(scrollElem).stop().fadeTo(300, 0); // fade out
		}
	});

	// Scroll to top animation on click
	jQuery(scrollElem).click(function(){ 
		jQuery('html, body').animate({scrollTop:100}, scrollSpeed); return false; 
	});

}); 


/* Anchorlink smooth scroll */
jQuery(function($) {
  jQuery('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
        || location.hostname == this.hostname) 
    {
      
      var target = jQuery(this.hash),
      headerHeight = jQuery("#header").height() + 5; // Get fixed header height
            
      target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
              
      if (target.length) 
      {
        jQuery('html,body').animate({
          scrollTop: target.offset().top - 100
		}, 1100,'swing');
        return false;
      }
    }
  });
});

var viewport_width = jQuery(window).width();/* getting viewport width */
var viewport_breakpoint_mobile_landscape = 480;
var viewport_breakpoint_tablets = 760;
var viewport_breakpoint_desktop = 1014;

jQuery(document).ready(function($) {
	//all
	init_external_links();
	init_linkables();
	init_animation();
	
	
	//smoth anclas
	init_smooth_anclas();

	if (viewport.width > viewport_breakpoint_tablets) {
	}
	
  
	// mobile 0 ... 760
	if (viewport.width < viewport_breakpoint_tablets) {
		init_menu_mobile();
		
	}
	
	// 961 ... ? desktop & tablets horitzontals
	else{
		
		init_menu_desktop();
		
	}
	
});



/* as page reload */
jQuery(window).resize(function () {
	var isDesktopBefore = true;
	var isDesktopAfter = true;
	// mirem si estem en versio mobile o desktop abans i despres de resizar
	if (viewport_width < viewport_breakpoint_tablets) {
				isDesktopBefore = false;
	}
	viewport = updateViewportDimensions(); 

    // we wait the set amount (in function above) then fire the function
    waitForFinalEvent( function() {
		if (viewport.width < viewport_breakpoint_tablets) {
					isDesktopAfter = false;
					
		}
		// si canviem de versió obliguem a canviar els menús:
		if (isDesktopAfter != isDesktopBefore) {
			if (viewport.width < viewport_breakpoint_tablets ) {
				init_menu_mobile();
				
				//console.log('canviem a mobile');
			} else {
				//console.log('canviem a desktop');
				init_menu_desktop();
				
			}
		}
		
	
		
		viewport_width = viewport.width; //actualitzem el valor de la variable viewport_width per si es torna a canviar el tamany de la finestra sense recarregar la pàgina

						
					 
     }, timeToWaitForLast, "your-function-identifier-string"); 
  });
 /* end as page reload */
 
 


function init_smooth_anclas(){
	jQuery('a.smooth').on('click', function(e) {
	var $link = jQuery(this);
	var anchor  = $link.attr('href');
	jQuery('html, body').stop().animate({
        scrollTop: jQuery(anchor).offset().top
	}, 1000);
	});
}


function init_linkables(){
	jQuery('.linkable').css('cursor','pointer').on('click',function(){
		var link = jQuery(this).find("a").eq(0);
		window.location.href = link.attr("href");
		
	});
}

 
 
 function init_menu_mobile(){
	var $main_menu = jQuery('#menu-container');
	var $toggle_menu = jQuery('#toggle-menu');
	$main_menu.hide();
	$toggle_menu.show();	
	$toggle_menu.off().on('click',function(event){
		event.preventDefault();
		$main_menu.toggle();
		jQuery(this).toggleClass('active');
	});
	
	
	//submenu
	/*
	jQuery('>li>a',$main_menu).off().on('click',function(event){
		if (jQuery('ul',this.parentNode).size() >0 ) {
			event.preventDefault();
			var toggeable = jQuery('ul.sub-menu',this.parentNode);
			toggeable.toggle();
		}
	});
	*/
	
}
function init_menu_desktop(){
	jQuery('#menu-container').show();
	jQuery('#toggle-menu').hide();	
}
function init_external_links(){
	jQuery('a.external').attr("target","_blank");
}

jQuery(window).on('scroll', function(){
	if(!scrolling){
	onScrollInit( jQuery('.os-animation') );
	scrolling = true;
	}
});
var scrolling = false;
function onScrollInit( items, trigger ) {
  items.each( function() {
    var osElement = jQuery(this),
        osAnimationClass = osElement.attr('data-os-animation'),
        osAnimationDelay = osElement.attr('data-os-animation-delay');
      
        osElement.css({
          '-webkit-animation-delay':  osAnimationDelay,
          '-moz-animation-delay':     osAnimationDelay,
          'animation-delay':          osAnimationDelay
        });

        var osTrigger = ( trigger ) ? trigger : osElement;
        
        osTrigger.waypoint(function(direction) {
			if (direction == "down") {
				osElement.addClass('animated').addClass(osAnimationClass);
			}else{
				//osElement.removeClass('animated').removeClass(osAnimationClass);
			}
		  },{
              triggerOnce: true,
              offset: '90%'
        });
  });
}

function animacio_logo(){
	if (jQuery('body').hasClass('home')) {
		var $content = jQuery('.slider-mod');
		var $logo = jQuery('#logo');
		$logo.addClass('active');
		$content.waypoint(function(direction){
			if (direction == "down") {
				$logo.removeClass('active');
			}else{
				$logo.addClass('active');	
			}
		});
	}
}

function init_animation(){
	animacio_logo();//animació del logo a la home quan els scroll està damunt del slider (utilitzant plugin waypoint)
	
	jQuery('body').addClass('content-loaded');
	jQuery('.animation').each(function(){
		//elements que mourem al carregar la pàgina
		var osElement = jQuery(this),
		osAnimationClass = osElement.attr('data-os-animation'),
		osAnimationDelay = osElement.attr('data-os-animation-delay');
	  
		osElement.css({
		  '-webkit-animation-delay':  osAnimationDelay,
		  '-moz-animation-delay':     osAnimationDelay,
		  'animation-delay':          osAnimationDelay
		});
	
		osElement.addClass('animated').addClass(osAnimationClass);
	
	});
	
}

/*
function test_waypoint(){
	var $activos_mod = jQuery('.activos-mod');
	$activos_mod.waypoint(function(direction){
		if (direction == "down") {
			$activos_mod.addClass();
		}
	},{offset: '80%'});
}
*/

/*
 * Get Viewport Dimensions
 * returns object with viewport dimensions to match css in width and height properties
 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
*/
function updateViewportDimensions() {
	var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],x=w.innerWidth||e.clientWidth||g.clientWidth,y=w.innerHeight||e.clientHeight||g.clientHeight;
	return { width:x,height:y }
}
// setting the viewport width
var viewport = updateViewportDimensions();


/*
 * Throttle Resize-triggered Events
 * Wrap your actions in this function to throttle the frequency of firing them off, for better performance, esp. on mobile.
 * ( source: http://stackoverflow.com/questions/2854407/javascript-jquery-window-resize-how-to-fire-after-the-resize-is-completed )
*/
var waitForFinalEvent = (function () {
	var timers = {};
	return function (callback, ms, uniqueId) {
		if (!uniqueId) { uniqueId = "Don't call this twice without a uniqueId"; }
		if (timers[uniqueId]) { clearTimeout (timers[uniqueId]); }
		timers[uniqueId] = setTimeout(callback, ms);
	};
})();

// how long to wait before deciding the resize has stopped, in ms. Around 50-100 should work ok.
var timeToWaitForLast = 100;
