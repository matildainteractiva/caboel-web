<?php
/*
 Template Name: Contacta
*/
?>
<?php get_header(); ?>
<?php
	if (function_exists('icl_object_id')){
		$id_activos_page = icl_object_id(14,'page',true);
	}else{
		$id_activos_page = 14;
	}
?>
			<div id="content" class="contacta-page">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
								
								<div class="image">
									<img src="<?php bloginfo('template_url'); ?>/library/images/contacta/contacta.jpg" alt="Caboel">
								</div>
								<header class="article-header" os-animation" data-os-animation="fadeInRight" data-os-animation-delay=".2s">
									<div class="inner">
										<h1 class="page-title"><span><?php the_title(); ?></span></h1>
										
										<div class="description">
											<p><?php _e('Nuestras oficinas se encuentran en:','caboel'); ?></p>
											<div class="address" itemscope itemtype="http://schema.org/Organization">
												<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
												<p itemprop="streetAddress">C/Josep Irla i Bosch, 1-3, 7ª planta</p>
												<p>
													<span itemprop="addressLocality">Barcelona</span>
													<span itemprop="postalCode">08034</span>
														
												</p>
												<meta itemprop="addressCountry" content="España">
												<meta itemprop="addressRegion" content="Catalunya">
												</div>
												Tel. <a href="tel:+34932058196" itemprop="telephone">+34 93 205 81 96</a><br>
												Fax. <a href="tel:+34932058197" itemprop="telephone">+34 93 205 81 97</a><br>
												<a href="mailto:info@caboel.es" itemprop="email">info@caboel.es</a><br>
												<meta itemprop="name" content="Caboel S.L.">
												<meta itemprop="image" content="<?php bloginfo('template_url');?>/library/images/logo-caboel.png">
												<meta itemprop="url" content="http://www.caboel.es/">
											</div>
										</div>
									</div>
								</header>
								
								<section class="module map-mod">
									
									<div class="mod-header">
										<h3 class="mod-title"><?php _e('Dónde estamos','caboel');?></h3>
										<a href="https://www.google.com/maps/dir//41.391002,2.129329/@41.3910821,2.1269464,17z/data=!3m1!4b1" class="external"><?php _e('Ver en Google Maps','caboel');?></a>
									</div>
									<div id="map-canvas"></div>
									
										<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCA82fELdVbRadtkV6Yi8I6-G-STicnHpE"></script>
									
										<script>
										  function initialize() {
											var mapCanvas = document.getElementById('map-canvas');
											var mapOptions = {
											  center: new google.maps.LatLng(41.391002, 2.129329),
											  zoom: 17,
											  mapTypeId: google.maps.MapTypeId.ROADMAP
											}
											var map = new google.maps.Map(mapCanvas, mapOptions);
											
											map.set('styles', [{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"weight":0.1},{"hue":"#828282"},{"saturation":-28}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"weight":5.2},{"hue":"#cec48e"},{"saturation":31},{"lightness":-13},{"gamma":3.3}]},{"featureType":"all","elementType":"all","stylers":[{"visibility":"on"},{"hue":"#c9ba6d"},{"saturation":19},{"lightness":17}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"visibility":"off"},{"invert_lightness":false},{"hue":"#8db0cc"},{"saturation":21},{"lightness":-26},{"gamma":1.95}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#608fab"},{"weight":0.16},{"hue":"#000000"},{"saturation":0}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"weight":0.63}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#adc2d5"},{"saturation":-13}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#cec48e"},{"weight":1.85},{"saturation":-27},{"lightness":34},{"gamma":2.04}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"invert_lightness":true},{"color":"#4f4f4f"},{"saturation":-100},{"lightness":52},{"gamma":2.51}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#406d80"},{"weight":0.95},{"lightness":-14}]},{"featureType":"administrative.locality","elementType":"labels.text","stylers":[{"visibility":"on"},{"color":"#171717"},{"weight":0.1}]},{"featureType":"transit.station.rail","elementType":"labels.icon","stylers":[{"visibility":"on"},{"weight":0.95},{"hue":"#3d5062"},{"saturation":14}]},{"featureType":"transit.station.rail","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#171717"},{"weight":3.3},{"saturation":-30}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"weight":1.78}]}]);
											
											var marker = new google.maps.Marker({
											  position:  {
														  "lat" : 41.391002,
														  "lng" : 2.129329
													   },
											  map: map,
											  icon: '<?php bloginfo('template_url');?>/library/images/icon-location.png',
											  title: 'CABOEL',
											  animation: google.maps.Animation.DROP
											});
											
											google.maps.event.addDomListener(window, "resize", function() {
												var center = map.getCenter();
												google.maps.event.trigger(map, "resize");
												map.setCenter(center); 
											});
											
											var contentString = '<div class="infowindow">'+
												'<h3>CABOEL,S.L.</h3>'+
												'<div class="infowindow-content">'+
												'<p>C/Josep Irla i Bosch, 1-3, 7ª planta</p>'+
												'<p> Barcelona 08034 </p>'+
												'</div>'+
												'</div>';
										
											var infowindow = new google.maps.InfoWindow({
												content: contentString,
												maxWidth: 200
											});
										  
											
											google.maps.event.addListener(marker, 'click', function() {
											  infowindow.open(map,marker);
											});
										  }
										
										  
										google.maps.event.addDomListener(window, 'load', initialize);
									
										 
										</script>
										
										<div class="getthere-mod os-animation" data-os-animation="fadeInUp" data-os-animation-delay=".2s">
											<form action="http://maps.google.com/maps"="get" target="_blank">
											<input type="hidden" name="saddr" value="">
											<input type = "hidden" name = "daddr" value = "41.391002, 2.129329">
											<button type="submit" class="btn submit"><?php _e('Cómo llegar hasta aquí','caboel');?></button>
											</form>
										</div>
										
									</section>
									
									
								
								
								
								

								

							</article>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page-custom.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</main>


				</div>

			</div>


<?php get_footer(); ?>
