<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'caboel_wp_bdd');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'caboel_wp_usr');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'gFrc766?');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'x.%Z&jqt+iU]e<-6o6A,wS>T5ZyQx^+eW-v8n`||Gyd=6-$DLu7),[UiSo4eCMq!');
define('SECURE_AUTH_KEY',  '|;mt?&B5[ye3kS92i,Cn_E60/mS#zZm26q5BlL ]J(ev6G/oj+:+:*2F<`*$@D,v');
define('LOGGED_IN_KEY',    ';FrL/|+e|n{AAp2BBNC^HACxZ8dO+C&|b$ *Of~x(7iKRM/Af.gfJ2Bup!JMlblc');
define('NONCE_KEY',        '14 FOq>.zd[d/s,m>ICC(AE35q9d?U[r<HP1(*aD(Sn<vA3D{y]R!Usn|&x&vMf(');
define('AUTH_SALT',        'c7pb/?#~vWS!V;S|~Y9(gni94rj.=;jU~;YVtdJ|c<dHx.@acC zYC|9ScIfvd|N');
define('SECURE_AUTH_SALT', 'O-WL1l(Ckv(b|$IjWx`I(7$uK?^3-%h7(kXSE-n|%9+mbgdd@i[`5I(}#NORZ(-A');
define('LOGGED_IN_SALT',   '*-pW|,h,Nd>[:vCzo!vzJqofuO||e-UZkm<>cvx&R=eSU>$I+}UV~g,Y)-~ P~xC');
define('NONCE_SALT',       '%i{B=|g6x.6vRkA|V6I0Ur}OP#UE*L70<>|R{As!bedJnV^/#^^#6JL]kXwYG=SV');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

define('WP_MEMORY_LIMIT', '256M');

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

